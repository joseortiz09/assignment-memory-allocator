#include "mem.h"


void test1(){
    void *heap = heap_init(1000);
    debug_heap(stdout, heap);
    printf("Test #1: creating the heap\n");
}

void test2(){
    void *buf = _malloc(500);
    debug_heap(stdout, HEAP_START);
    printf("Test #2: allocating a memory block\n");
    _free(buf);
}

void test3(){
    void *buf2 = _malloc(200);
    void *buf3 = _malloc(500);
    void *buf4 = _malloc(500);
    void *buf5 = _malloc(100);
    _free(buf4);
    _free(buf3);
    debug_heap(stdout, HEAP_START);
    printf("Test #3: freeing a block\n");
    _free(buf2);
    _free(buf5);
}

void test4(){
    void *huge = _malloc(7000);
    debug_heap(stdout, HEAP_START);
    printf("Test #4: allocating huge blocks\n");
    _free(huge);
}




int main(){
    test1();
    test2();
    test3();
    test4();
}
